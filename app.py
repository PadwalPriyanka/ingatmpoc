from flask import Flask ,jsonify, request, json,make_response
from flask_pymongo import PyMongo
import requests
from flask_cors import CORS
import jwt
import datetime
from functools import wraps

app = Flask(__name__)
CORS(app) ## To allow direct AJAX calls

app.config['MONGO_DBNAME']='atmpoc'
app.config['MONGO_URI']='mongodb://atmuser:atmpassword1@ds147207.mlab.com:47207/atmpoc?retryWrites=false'
app.config['SECRET_KEY'] = 'thisisthesecretkey'

mongo = PyMongo(app)

# Flask Authentication Application

def token_required(f):
    @wraps(f)
    def decorated(*args,**kwargs):
        #token = request.args.get('token') 
        token = request.headers.get('Token')
        if not token:
            return jsonify({'message' : 'Token is Missing'}),403

        try:
            data = jwt.decode(token,app.config['SECRET_KEY'])
        except:
            return jsonify({'message' : 'Token invalid'}),403
        
        return f(*args,**kwargs)
        
    return decorated


@app.route('/login')
def login():
    auth = request.authorization

    if auth and auth.password == 'password':
        token = jwt.encode({'user': auth.username,'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)},app.config['SECRET_KEY'])
        
        return jsonify({'token': token.decode('UTF-8')})

    return make_response('Could not verify!',401,{'www-Authenticate':'Basic realm="Login Required"'})


@app.route('/get_atms', methods=['GET'])
@token_required
def get_all_atmlists():
    atmlist = mongo.db.atmlist
    output=[]  
    if atmlist and request.method == 'GET': 
        '''Check for json response if it is available in mongodb, 
        Then just return from DataBase'''

        for item in atmlist.find():
            item['_id'] = str(item['_id'])
           
            if item["address"]["city"] == request.args['city']:
                ''' Append data from MongoDB '''
                output.append(item)   
        resp = jsonify({'result': output}) 

        ''' Data is not present in Mongo DB search in external API'''
        if output == []:
            externaloutput=[]
            external_json   = requests.get('https://www.ing.nl/api/locator/atms/')
            
            ''' Data is incorrect removing startting character '''
            apiResponse  = external_json.text[6:]
            atmListJSON = json.loads(apiResponse)

            '''Filter the response for requested city and save that json in mongodb'''
            for atm in atmListJSON:
                if atm["address"]["city"] == request.args['city']:

                    ''''Store Data in the MongoDb''' 
                    mongo.db.atmlist.insert_one(atm).inserted_id

                    '''ObjectId is not in Json format before append remove the _id'''
                    atm['_id'] = str(atm['_id']) 
                    
                    externaloutput.append(atm)    
            resp = jsonify({'result': externaloutput}) 
            return resp     
        else:
            resp.status_code = 200
            return resp


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status' : 404,
        'message' : 'Not Found' + request.url
    }

    resp = jsonify(message)
    resp.status_code = 404
    return resp



if __name__ == "__main__":
    app.run(debug=True)